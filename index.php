<?php

$a=1;
echo gettype($a);
echo "</br>";
?>

<?php

$a=1.5;
echo gettype($a);
echo "</br>";
?>

<?php

$a=null;
echo gettype($a);
echo "</br>";
?>
<?php

$a='Nasrin';
echo gettype($a);
echo "</br>";
?>
<?php

$a=new stdClass();
echo gettype($a);
echo "</br>";
?>
<?php
$a=new stdClass;
echo gettype($a);
echo "</br>";
?>
<?php
$a=array();
echo gettype($a);
echo "</br>";
?>

<?php
$a=array(1,1.5,null,"nasrin",new stdClass(),array());
foreach($a as $value) {
    echo "</br>";
    echo gettype($value);
}
?>




